#!/usr/bin/env python3

from aws_cdk import core

from cdk_sqs_lambda_iam.cdk_sqs_lambda_iam_stack import CdkSqsLambdaIamStack


app = core.App()
CdkSqsLambdaIamStack(app, "cdk-sqs-lambda-iam", env={'region': 'eu-central-1'})

app.synth()
