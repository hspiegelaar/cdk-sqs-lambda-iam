from aws_cdk import core
from aws_cdk import aws_sqs as sqs
from aws_cdk import aws_iam as iam
from aws_cdk import aws_lambda as lambda_
from aws_cdk import aws_ec2 as ec2
from aws_cdk import aws_efs as efs


class CdkSqsLambdaIamStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        account_id = '945241922667'

        # -- declare the queue
        queue_name = 'hoi1'
        my_queue = sqs.Queue(self, id='hoi1', queue_name='hoi1')

        policy_to_post_to_sqs = iam.PolicyStatement(
            actions=['sqs:SendMessage'],
            resources=[f'arn:aws:sqs:eu-central-1:{account_id}:{queue_name}']
        )

        # -- declare the VPC, EFS and access_point
        my_vpc = ec2.Vpc(self,
                         "my_cdk_vpc",
                         cidr="10.0.0.0/16",
                         nat_gateways=None,
                         max_azs=1,
                         vpn_gateway=None
                         )

        my_efs = efs.FileSystem(self,
                                'my_cdk_efs',
                                vpc=my_vpc,
                                removal_policy=core.RemovalPolicy.DESTROY
                                )

        my_access_point = my_efs.add_access_point('my_cdk_access_point',
                                                  create_acl=efs.Acl(owner_gid='1001', owner_uid='1001',
                                                                     permissions='777'),
                                                  path="/",
                                                  posix_user=efs.PosixUser(gid="1001", uid="1001")
                                                  )

        # -- declare the lambda
        my_lambda = lambda_.Function(
            self, 'my_lambda_name_1',
            function_name="my_lambda_from_cdk",
            code=lambda_.Code.asset("cdk_sqs_lambda_iam/src/"),
            handler="lambda_function.event_handler",
            runtime=lambda_.Runtime.PYTHON_3_8,
            memory_size=1792,
            environment={
                'queue_url': f'https://sqs.eu-central-1.amazonaws.com/{account_id}/{queue_name}'
            },
            tracing=lambda_.Tracing('ACTIVE'),
            vpc=my_vpc,
            filesystem=lambda_.FileSystem.from_efs_access_point(my_access_point, '/mnt/efs')
        )

        my_lambda.add_to_role_policy(policy_to_post_to_sqs)
