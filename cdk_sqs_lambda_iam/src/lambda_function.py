import boto3
import os
# import datetime as dt


def event_handler(event, context):
    queue_url = os.environ.get('queue_url')

    print(event)

    print(os.listdir('/mnt/efs'))

    queue = boto3.client('sqs')

    # t1 = dt.datetime.now()
    # t2 = dt.datetime.now()

    response = queue.send_message(
        QueueUrl=queue_url,
        MessageBody=event.get('key1', 'No entry found')
    )

    # t3 = dt.datetime.now()

    # response = queue.send_message(
    #     QueueUrl=queue_url,
    #     MessageBody=event.get('non_existing key', 'No entry found')
    # )
    #
    # t4 = dt.datetime.now()
    #
    # print(f'time between two time calls: {t2 - t1}')
    #
    # print(f'eerste post: {t3 - t2}')
    #
    # print(f'tweede post: {t4 - t3}')
